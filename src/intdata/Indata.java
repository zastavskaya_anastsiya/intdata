package intdata;

import java.io.*;


/**
 * Создаёт файл целых чисел,заполненый числами из текстовика
 * @author Заставская
 *
 */
public class Indata {
    public static void main(String[] args) throws IOException {
        DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream("indata.dat"));
        BufferedReader reader = new BufferedReader(new FileReader("num.txt"));
        String num;

        while ((num = reader.readLine())!= null){
            dataOutputStream.writeInt(Integer.valueOf(num));
        }
         dataOutputStream.close();
         reader.close();


    }
}
