package intdata;

import java.io.*;

/**
 * Метод для отбора из шестизначных чисел "счасливые".
 * @author Заставская
 */
public class Txt6data {
    public static void main(String[]args) throws IOException{
        DataInputStream dataInput = new DataInputStream(new FileInputStream("in6data.dat"));//чтение
        BufferedWriter writer = new BufferedWriter(new FileWriter("txt6data.dat"));//запись

        while (dataInput.available()>0) {
           int num ;
            num = dataInput.readInt();
                int n1 = num % 10;
                int n2 = num % 100;
                n2 = n2 / 10;
                int n3 = num % 1000;
                n3 = n3 / 100;
                int n4 = num % 10000;
                n4 = n4 / 1000;
                int n5 = num % 100000;
                n5 = n5 / 10000;
                int n6 = num % 1000000;
                n6 = n6 / 100000;
                int d = n1 + n2 + n3;
                int f = n4 + n5 + n6;
                if (d == f) {
                    writer.write(String.valueOf(num));
                    writer.write(" ");
                }
        }
        dataInput.close();
        writer.close();
    }
}
