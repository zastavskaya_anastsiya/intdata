package intdata;

import java.io.*;
/**
 * Метод для выбора шестизначных чисел
 * @author Заставская 15ОИТ18
 */
public class In6data  {
    public static void main(String[]args) throws IOException {
        DataOutputStream dataOutput = new DataOutputStream(new FileOutputStream("in6data.dat"));//запись
        DataInputStream dataInputStream = new DataInputStream(new FileInputStream("indata.dat"));//чтение
        int num;

        while (dataInputStream.available()>0){
            num = dataInputStream.readInt();
            if(num<1000000 && num>99999){
                dataOutput.writeInt(num);

            }
        }
        dataInputStream.close();
        dataOutput.close();

    }


}
