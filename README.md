Добавлено:

1. Программным способом создание файла целых чисел intdata.dat.
2. На основе данных файла intdata.dat сформирован файл, содержащий только шестизначные
целые числа int6data.dat.
3. На основе файла int6data.dat сформирован текстовый файл txt6data.dat, содержащий только
счастливые номера (сумма левых 3-х цифр равна сумме правых 3-х цифр).
![num.png](https://bitbucket.org/repo/baannzq/images/832060484-num.png)
![number.jpg](https://bitbucket.org/repo/baannzq/images/3723330869-number.jpg)